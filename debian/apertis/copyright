Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1999, 2001, 2002, 2006, 2007, 2009-2023, Free Software Foundation
 1992, 1993, Jean-loup Gailly
License: GFDL-1.3+ or GPL

Files: GNUmakefile
 Makefile.in
 cfg.mk
 dfltcc.c
 gunzip.in
 maint.mk
 zcat.in
 zcmp.in
 zless.in
Copyright: 1985, 1986, 1988, 1990-2023, Free Software Foundation, Inc.
License: GPL-3+

Files: INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2017, 2020-2023, Free Software
License: FSFAP

Files: Makefile.am
 inflate.c
Copyright: 1995, 1997-1999, 2001-2003, 2006, 2007, 2009-2023, Free Software Foundation
License: GPL-3+

Files: NEWS
Copyright: 1999, 2001, 2002, 2006, 2007, 2009-2023, Free Software Foundation
 1992, 1993, Jean-loup Gailly
License: GFDL-1.3+

Files: TODO
Copyright: 1999, 2001, 2006, 2009-2023, Free Software Foundation, Inc.
 1992, 1993, Jean-loup Gailly
License: GFDL-1.3+

Files: aclocal.m4
Copyright: 1996-2023, Free Software Foundation, Inc.
License: FSFULLR or GPL or LGPL

Files: bits.c
 configure.ac
 deflate.c
 trees.c
 unpack.c
 unzip.c
 zdiff.in
 zforce.in
 zip.c
 zmore.in
Copyright: 1997-2002, 2006, 2007, 2009-2023, Free Software Foundation, Inc.
 1992, 1993, Jean-loup Gailly
License: GPL-3+

Files: build-aux/*
Copyright: 1985, 1986, 1988, 1990-2023, Free Software Foundation, Inc.
License: GPL-3+

Files: build-aux/ar-lib
 build-aux/compile
 build-aux/depcomp
 build-aux/mdate-sh
 build-aux/missing
 build-aux/test-driver
Copyright: 1995-2023, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: build-aux/gnupload
Copyright: 2004-2023, Free Software Foundation, Inc.
License: GPL-2+

Files: build-aux/install-sh
Copyright: 1994, X Consortium
License: X11

Files: configure
Copyright: 1992-1996, 1998-2017, 2020-2023, Free Software Foundation
License: FSFUL

Files: debian/*
Copyright: 1995-2017, Bdale Garbee <bdale@gag.com>
License: GPL-3+

Files: debian/patches/*
Copyright: 1998-1999, 2001-2002, 2006-2007, 2009-2023, Free
 1998, 1999, 2001, 2002, 2006, 2007, 2009-2023, Free
License: GPL-3+

Files: debian/patches/disable-Werror.patch
Copyright: 1995-2017, Bdale Garbee <bdale@gag.com>
License: GPL-3+

Files: doc/Makefile.am
 doc/Makefile.in
Copyright: 1985, 1986, 1988, 1990-2023, Free Software Foundation, Inc.
License: GPL-3+

Files: doc/gzip.info
Copyright: 1998, 1999, 2001, 2002, 2006, 2007, 2009-2023, Free Software
 1992, 1993, Jean-loup Gailly
License: GFDL-1.3+

Files: doc/gzip.texi
Copyright: 1998-1999, 2001-2002, 2006-2007, 2009-2023, Free
 1992, 1993 Jean-loup Gailly
License: GFDL-1.3+

Files: gzexe.in
 gzip.c
 util.c
Copyright: 1997-1999, 2001, 2002, 2004, 2006, 2007, 2009-2023, Free Software
 1992, 1993, Jean-loup Gailly
License: GPL-3+

Files: gzip.h
 revision.h
Copyright: 1997-1999, 2001, 2002, 2006, 2007, 2009-2023, Free Software
 1992, 1993, Jean-loup Gailly.
License: GPL-3+

Files: lib/*
Copyright: 1987-2023, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: lib/Makefile.am
 lib/Makefile.in
 lib/at-func.c
 lib/chdir-long.c
 lib/chdir-long.h
 lib/creat-safer.c
 lib/dirent--.h
 lib/dirent-safer.h
 lib/dup-safer-flag.c
 lib/dup-safer.c
 lib/fcntl--.h
 lib/fcntl-safer.h
 lib/fd-safer-flag.c
 lib/fd-safer.c
 lib/fdopendir.c
 lib/gnulib.mk
 lib/open-safer.c
 lib/openat-die.c
 lib/openat-priv.h
 lib/openat-proc.c
 lib/openat-safer.c
 lib/openat.c
 lib/openat.h
 lib/opendir-safer.c
 lib/pipe-safer.c
 lib/unistd--.h
 lib/unistd-safer.h
 lib/unlinkat.c
 lib/xalloc.h
 lib/xmalloc.c
 lib/yesno.c
 lib/yesno.h
Copyright: 1985, 1986, 1988, 1990-2023, Free Software Foundation, Inc.
License: GPL-3+

Files: lib/_Noreturn.h
 lib/arg-nonnull.h
 lib/c++defs.h
 lib/warn-on-use.h
Copyright: 2009-2023, Free Software Foundation, Inc.
License: LGPL-2+

Files: lib/alloca.in.h
 lib/basename-lgpl.h
 lib/dirname.h
 lib/error.in.h
 lib/stripslash.c
Copyright: 1990, 1995-1999, 2001-2023, Free Software Foundation
License: LGPL-2.1+

Files: lib/basename-lgpl.c
 lib/dirname-lgpl.c
 lib/filenamecat.h
 lib/gettext.h
 lib/minmax.h
 lib/pathmax.h
 lib/printf-args.c
 lib/printf-args.h
 lib/printf-parse.h
 lib/realloc.c
 lib/rmdir.c
 lib/stpcpy.c
 lib/strdup.c
Copyright: 1988, 1990-1992, 1995-2007, 2009-2023, Free Software
License: LGPL-2.1+

Files: lib/fchdir.c
 lib/fprintf.c
 lib/gettime.c
 lib/printf.c
 lib/timespec.c
 lib/utime.c
 lib/utimens.c
 lib/utimens.h
 lib/vfprintf.c
Copyright: 2002-2023, Free Software Foundation, Inc.
License: LGPL-3+

Files: lib/match.c
Copyright: 1997-2002, 2006, 2007, 2009-2023, Free Software Foundation, Inc.
 1992, 1993, Jean-loup Gailly
License: GPL-3+

Files: lib/memrchr.c
 lib/timespec.h
Copyright: 1991, 1993, 1996, 1997, 1999, 2000, 2002-2023, Free Software
License: LGPL-3+

Files: lib/save-cwd.c
 lib/savedir.c
 lib/savedir.h
 lib/xalloc-die.c
Copyright: 1990, 1995, 1997-2006, 2009-2023, Free Software
License: GPL-3+

Files: lib/save-cwd.h
Copyright: 1995, 1997-1999, 2001-2003, 2006, 2007, 2009-2023, Free Software Foundation
License: GPL-3+

Files: lzw.h
Copyright: 1994-2023, Free Software Foundation, Inc.
 1992, 1993, Jean-loup Gailly.
License: GPL-3+

Files: m4/*
Copyright: 1992-2023, Free Software Foundation, Inc.
License: FSFULLR

Files: m4/alloca.m4
 m4/intmax_t.m4
 m4/malloca.m4
 m4/mbrtowc.m4
 m4/mempcpy.m4
 m4/memrchr.m4
 m4/pathmax.m4
 m4/savedir.m4
 m4/yesno.m4
Copyright: 1997-2023, Free Software Foundation
License: FSFULLR

Files: m4/codeset.m4
 m4/stat-time.m4
Copyright: 1998-2003, 2005-2023, Free Software
License: FSFULLR

Files: m4/gnulib-comp.m4
 m4/shell.m4
Copyright: 1985, 1986, 1988, 1990-2023, Free Software Foundation, Inc.
License: GPL-3+

Files: tailor.h
 zgrep.in
 znew.in
Copyright: 1997-1999, 2001, 2002, 2004, 2006, 2007, 2009-2023, Free Software Foundation
 1992, 1993, Jean-loup Gailly
License: GPL-3+

Files: tests/*
Copyright: 1985, 1986, 1988, 1990-2023, Free Software Foundation, Inc.
License: GPL-3+

Files: ChangeLog-2007
Copyright: 1999-2023 Free Software Foundation, Inc.
 1992-1993 Jean-loup Gailly and Mark Adler
License: GPL-3+

Files: doc/fdl.texi
Copyright: 1999-2023 Free Software Foundation, Inc. http://fsf.org/
 1992-1993 Jean-loup Gailly
License: GFDL-1.3+-no-invariant

Files: gzip.1 zless.1
Copyright: 1998-2023 Free Software Foundation, Inc.
 1992-1993 Jean-loup Gailly
License: FSF-manpages
